$(document).ready(function() {
    $.ajax({
        url: 'scripts/gallery.json',
        dataType: "json",
        method: "GET",
        complete: function(x) {
            
                                
            var c = $.parseJSON(x.responseText);
            
            
            $(".gallery img").click(function() {
                
                $("html, body").animate({
                    scrollTop: $('#portfolio .content').offset().top
                }, 1000);
                                
                $(".single-item").show().animate({
                    height: "410px",
                    }, 1000);
                
                item = $(this).attr("data-item");   
            
            
            $(".single-item .preview img").attr({
                src: "images/projects/"+c.gallery[item].id+".jpg",
                alt: c.gallery[item].fullname,
                title: c.gallery[item].fullname,
                class: c.gallery[item].type
            }).load(function() {
                $(".single-item").animate({"opacity": 1}, 500);
            });
            
                $(".single-item .desc h3").html(c.gallery[item].fullname);
                $(".single-item .desc p").html(c.gallery[item].desc);
                $(".single-item .desc span em").html(c.gallery[item].tech);
                $(".single-item .desc .web").html(c.gallery[item].online);
            });
            
            
            
        },
        error: function(err){
        }
        
    });
    
    
    
    $(".single-item .desc .close").click(function(){
                $(".single-item").animate({
                    height: "0px",
                    }, 1000, function() {
                    $(this).hide();
                });
            });
    
});