$(document).ready(function() {
    
   function portfolio(whata, show, hide1, hide2) {
        $('.'+whata+'').click(function() {
            $('.gallery img.' + show + '').fadeIn();
            $('.content ul li').removeClass('current');
            $(this).addClass("current");
            $('.gallery img.'+ hide1 + '').fadeOut("slow");
            $('.gallery img.'+ hide2 + '').fadeOut("slow");
        });
    }
        
    portfolio('il','illu','website','other');
    portfolio('webpages','website','illu','other');
    portfolio('others','other','website','illu');
    
    
    $('.all').click(function() {
        $('.gallery img').fadeIn();
        $('.content ul li').removeClass('current');
        $(this).addClass("current");     
    });
    
    // Wywołanie nawigacji dla mobilek SlickNav

    $('nav div ul').slicknav({
    label: '&#x2630;'});
    
    // Automatyczna zmiana roku w stopce (a.k.a. strona na lata)
    
    var year = new Date().getFullYear();
    
    $('.currentYear').append(year);
    
    // mini AntySpambot
    
    $("a.dotcom").attr("href","mailto:mateusz.michnowicz@gmail.com");
    $("a.dotcz").attr("href","mailto:mateusz@michnowi.cz");
    
    $(".arrow-up").click(function() {
        $("body").animate({
            scrollTop: $("nav").offset().top
        },1000);

    });
    
    
});

